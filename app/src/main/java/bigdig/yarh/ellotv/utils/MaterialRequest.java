package bigdig.yarh.ellotv.utils;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import bigdig.yarh.ellotv.data.Local;
import bigdig.yarh.ellotv.data.MainEntity;
import bigdig.yarh.ellotv.data.ProfileEntity;

public class MaterialRequest {

    private final JSONParser jsonParser;
    public static final  String SITE                  = "http://ello.tv/";
    private static final String
                                MAIN_URL              =
            SITE + Local.INSTANCE.lang + "api/home/video";
    public static final  String GET_PROFILE_API       = SITE + "api/profile";
    public static final  String SOCIAL_AUTHENTICATION = SITE + "api/auth/login";


    public MaterialRequest() {

        jsonParser = new JSONParser();
    }


    public List<MainEntity> getMain() {
        JSONObject json = jsonParser.getJSON(MAIN_URL);
        List<MainEntity> articles = null;
        if (json != null) try {
            articles = new ArrayList<>();
            JSONArray
                    items =
                    json.getJSONObject("data")
                            .getJSONArray(
                                    "items");
                for (int i = 0; i < items.length(); i++) {
                    MainEntity a = new MainEntity();
                    JSONObject j = items.getJSONObject(i);
                    a.setSlug(j.getString("slug"));
                    a.setTitle(j.getString("title"));
                    a.setImageUrl(j.getString("picture"));
                    int viewCount = 0;
                    if (j.has("view_count"))
                        viewCount = j.getInt("view_count");
                    a.setViewCount(" " + parseLongMumber(viewCount));
                    viewCount = 0;
                    if (j.has("like_count") && !j.getString("like_count").equals("null")) {
                        viewCount = j.getInt("like_count");
                    }
                    a.setFavoriteCount(" " + parseLongMumber(viewCount));
                    JSONArray artists = j.getJSONArray("artists");
                    String artistsNames = "";
                    for (int arS = 0; arS < artists.length(); arS++) {
                        JSONObject aJson = artists.getJSONObject(arS);
                        artistsNames += aJson.getString("name") + " ";
                    }
                    a.setArtist(artistsNames);
                    articles.add(a);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        return articles;

    }

    public ProfileEntity getProfile() {
        JSONObject json = jsonParser.getJSON(GET_PROFILE_API);
        ProfileEntity a = null;
        if (json != null)

            try {
                if (json.getBoolean("status")) {
                    JSONObject j = json.getJSONObject("data");
                    a = new ProfileEntity();
                    a.setId(j.getInt("id"));
                    a.setNickName(j.getString("nickName"));
                    a.setFirstNam(j.getString("firstName"));
                    a.setLastName(j.getString("lastName"));
                    a.setEmail(j.getString("email"));
                    try {
                        a.setGender(j.getInt("gender"));
                    } catch (JSONException e) {
                        Log.i("parse error", j.getString("gender"));
                        a.setGender(0);
                    }
                    try {
                        a.setCountry(j.getInt("country"));
                    } catch (JSONException e) {
                        Log.i("parse error", j.getString("country"));
                        a.setCountry(0);
                    }
                    try {
                        a.setCity(j.getString("city"));
                    } catch (JSONException e) {
                        Log.i("parse error", j.getString("city"));
                        a.setCity("");
                    }
                    // JSONObject date = j.getJSONObject("birthday");
                    // try {
                    // a.setYear(date.getInt("year"));
                    // } catch (JSONException e) {
                    // Log.i("parse error", j.getString("year"));
                    // a.setYear(0);
                    // }
                    // try {
                    // a.setMonth(date.getInt("month"));
                    // } catch (JSONException e) {
                    // Log.i("parse error", j.getString("month"));
                    // a.setMonth(0);
                    // }
                    // try {
                    // a.setDay(date.getInt("day"));
                    // } catch (JSONException e) {
                    // Log.i("parse error", j.getString("day"));
                    // a.setDay(0);
                    // }
                    a.setAvatar(j.getString("avatar"));
                    Map<String, Integer> social = new HashMap<>();
                    JSONArray socialArray = j.getJSONArray("social");
                    for (int i = 0; i < socialArray.length(); i++) {
                        JSONObject s = socialArray.getJSONObject(i);
                        social.put(s.getString("type"), s.getInt("id"));
                    }
                    a.setSocial(social);
                    a.setSubscribe(j.getBoolean("subscribe"));
                    a.setFollowers_count(j.getInt("followers_count"));
                    a.setFollowing_count(j.getInt("following_count"));
                } else {
                    String errorMessage = "";
                    if (json.has("error"))
                        errorMessage += json.getString("error") + " ";
                    if (json.has("message"))
                        errorMessage += json.getString("message");
                    ErrorMessage.INSTANCE.setErrorMessage(errorMessage);
                    return null;
                }
            } catch (JSONException e) {
                ErrorMessage.INSTANCE.parseErrorMessage();
            }
        return a;

    }

    private String parseLongMumber(int count) {
//        String parse = "" + count;
//        double devider = 0;
//        if (count >= 1000000000) {
//            parse = "" + count;
//            parse = parse.substring(0, parse.length() - 8);
//            devider = Double.parseDouble(parse);
//            devider /= 10;
//            parse = "" + devider;
//            parse += "b";
//        } else if (count >= 1000000) {
//            parse = "" + count;
//            parse = parse.substring(0, parse.length() - 5);
//            devider = Double.parseDouble(parse);
//            devider /= 10;
//            parse = "" + devider;
//            parse += "m";
//        } else if (count >= 1000) {
//            parse = "" + count;
//            parse = parse.substring(0, parse.length() - 2);
//            devider = Double.parseDouble(parse);
//            devider /= 10;
//            parse = "" + devider;
//            parse += "k";
//        } else if (count < 1000) {
//            parse = "" + count;
//        }
        return "" + count;
    }

}
