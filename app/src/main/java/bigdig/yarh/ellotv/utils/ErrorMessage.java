package bigdig.yarh.ellotv.utils;

public enum ErrorMessage {
    INSTANCE;
    private String errorMessage = "Не возможно подключится к серверу";


    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }


    public void parseErrorMessage() {
        errorMessage = "Ошибка обработки данных";
    }

}

