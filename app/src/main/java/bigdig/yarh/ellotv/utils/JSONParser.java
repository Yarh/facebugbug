package bigdig.yarh.ellotv.utils;

import android.util.Log;
import android.webkit.CookieManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class JSONParser {

	public JSONObject getJSON(String url) {
		Log.i("JSON parser url", url);
		String json = "";
		JSONObject jObj;
		HttpURLConnection c = null;

		CookieManager myCookieManager = CookieManager.getInstance();
		String cookies = myCookieManager.getCookie(url);
        if(cookies!=null) Log.d("cookies",cookies);
		try {
			URL u = new URL(url);
			c = (HttpURLConnection) u.openConnection();
			c.setRequestMethod("GET");
			c.setRequestProperty("Cookie", cookies);
			c.setConnectTimeout(6000);
			c.setReadTimeout(6000);
			// c.setDoOutput(true);
//			 c.setDoInput(true);
			c.setUseCaches(false);
			c.connect();

			int status = c.getResponseCode();
			Log.e("response status", "" + status);
			BufferedReader br = new BufferedReader(new InputStreamReader(
					c.getInputStream()));
			StringBuilder sb = new StringBuilder();
			String line;
			while ((line = br.readLine()) != null) {
				sb.append(line).append("\n");
			}
			br.close();
			String jsonLog =sb.toString();			json = (sb.toString());
			// cookies = CookieManager.getInstance().getCookie(url);
			// Log.d("cookie recieve  check", "All the cookies in a string:"
			// + cookies);

			if (jsonLog.length() > 1000) {
				Log.v("JSON response", "sb.length = " + jsonLog.length());
				int chunkCount = jsonLog.length() / 1000; // integer division
				for (int i = 0; i <= chunkCount; i++) {
					int max = 1000 * (i + 1);
					if (max >= jsonLog.length()) {
						Log.v("JSON response", ("chunk " + i + " of "
                                + chunkCount + ":" + jsonLog
                                .substring(1000 * i)));
					} else {
						Log.v("JSON response", ("chunk " + i + " of "
                                + chunkCount + ":" + jsonLog.substring(
                                1000 * i, max)));
					}
				}
			} else
				Log.v("JSON response", jsonLog);

		} catch (MalformedURLException ex) {
			Log.e("mailformed url", ex.toString());
		} catch (IOException ex) {
			Log.e("io exception", ex.toString());
		} finally {
			if (c != null)
				c.disconnect();
		}
		try {
			jObj = new JSONObject(json);
		} catch (JSONException e) {
			Log.e("JSON Parser", "Error parsing data " + e.toString()
                    + "JSON: " + json);
			ErrorMessage.INSTANCE.parseErrorMessage();
			return null;
		}
		// if (jObj != null) {
		// Log.d("JSON", jObj.toString());
		// }

		return jObj;
	}

}