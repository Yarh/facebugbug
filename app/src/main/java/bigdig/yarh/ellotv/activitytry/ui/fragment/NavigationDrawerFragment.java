package bigdig.yarh.ellotv.activitytry.ui.fragment;

import android.app.ActionBar;
import android.app.Activity;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.CookieManager;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.ToggleButton;


import bigdig.yarh.ellotv.activitytry.R;
import bigdig.yarh.ellotv.activitytry.ui.activity.NavigationDrawerActivity;
import bigdig.yarh.ellotv.adapter.DrawerItemCustomAdapter;
import bigdig.yarh.ellotv.data.FragmentPosition;
import bigdig.yarh.ellotv.data.LoginState;
import bigdig.yarh.ellotv.data.ProfileEntity;
import bigdig.yarh.ellotv.widget.TypefacedTextView;

/**
 * Fragment used for managing interactions for and presentation of a navigation drawer.
 * See the <a href="https://developer.android.com/design/patterns/navigation-drawer.html#Interaction">
 * design guidelines</a> for a complete explanation of the behaviors implemented here.
 */
public class NavigationDrawerFragment
        extends Fragment {

    /**
     * Per the design guidelines, you should show the drawer on launch until the user manually
     * expands it. This shared preference tracks this.
     */
    private static final String PREF_USER_LEARNED_DRAWER = "navigation_drawer_learned";

    /**
     * A pointer to the current callbacks instance (the Activity).
     */
    private NavigationDrawerCallbacks mCallbacks;

    /**
     * Helper component that ties the action bar to the navigation drawer.
     */
    private ActionBarDrawerToggle mDrawerToggle;

    private DrawerLayout mDrawerLayout;
    private ListView     mDrawerListView;
    private View         mFragmentContainerView;

    private boolean           mUserLearnedDrawer;
    private ImageView         imAvatar;
    private Button            btnLogin;
    private LinearLayout      llLogged;
    private LinearLayout      llUnLogged;
    private LinearLayout      llProfileDetails;
    private TypefacedTextView profileName;
    private ToggleButton      tbProfile;
    private TextView          tvFollowersCount;
    private TextView          tvFollowingCount;


    public NavigationDrawerFragment() {
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(getActivity());
        mUserLearnedDrawer = sp.getBoolean(PREF_USER_LEARNED_DRAWER, false);

    }


    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        setHasOptionsMenu(true);
    }





    private Button btnBackMenu;
    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState
    ) {
        View view = inflater.inflate(
                R.layout.fragment_navigation_drawer2, container, false
        );
        mDrawerListView = (ListView) view.findViewById(R.id.left_drawer);
        mDrawerListView.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
        DrawerItemCustomAdapter adapter = new DrawerItemCustomAdapter(
                getActivity(), getResources().getStringArray(
                R.array.navigation_drawer_items_array
        ), getResources().getStringArray(
                R.array.navigation_drawer_social_array
        )
        );
        mDrawerListView.setAdapter(adapter);
        imAvatar = (ImageView) view.findViewById(R.id.im_avatar);
        llLogged = (LinearLayout) view.findViewById(R.id.ll_logged);
        llUnLogged = (LinearLayout) view.findViewById(R.id.ll_unlogged);
        llProfileDetails = (LinearLayout) view.findViewById(R.id.ll_profile_details);
        tvFollowersCount = (TextView) view.findViewById(R.id.tv_followers_count);
        tvFollowingCount = (TextView) view.findViewById(R.id.tv_following_count);
        profileName = (TypefacedTextView) view.findViewById(R.id.tv_profile_name);
        tbProfile = (ToggleButton) view.findViewById(R.id.tb_profile);
        btnLogin = (Button) view.findViewById(R.id.btn_login);
        tbProfile.setOnCheckedChangeListener(tbProfileListener);
        btnLogin.setOnClickListener(openAuthentication);

                selectItem(FragmentPosition.INSTANCE.position);
        btnBackMenu = (Button) view.findViewById(R.id.btn_back_to_menue);
        return view;
    }


    public boolean isDrawerOpen() {
        return mDrawerLayout != null && mDrawerLayout.isDrawerOpen(mFragmentContainerView);
    }


    /**
     * Users of this fragment must call this method to set up the navigation drawer interactions.
     *
     * @param fragmentId   The android:id of this fragment in its activity's layout.
     * @param drawerLayout The DrawerLayout containing this fragment's UI.
     */
    public void setUp(int fragmentId, DrawerLayout drawerLayout) {
        mFragmentContainerView = getActivity().findViewById(fragmentId);
        mDrawerLayout = drawerLayout;

        // set a custom shadow that overlays the main content when the drawer opens
        mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);
        // set up the drawer's list view with items and click listener

        ActionBar actionBar = getActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeButtonEnabled(true);

        // ActionBarDrawerToggle ties together the the proper interactions
        // between the navigation drawer and the action bar app icon.
        mDrawerToggle = new ActionBarDrawerToggle(
                getActivity(),                    /* host Activity */
                mDrawerLayout,                    /* DrawerLayout object */
                R.drawable.ic_drawer_blue,
                R.string.navigation_drawer_open,  /* "open drawer" description for accessibility */
                R.string.navigation_drawer_close  /* "close drawer" description for accessibility */
        ) {

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                if (!isAdded()) {
                    return;
                }

                getActivity().invalidateOptionsMenu(); // calls onPrepareOptionsMenu()
            }


            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                if (!isAdded()) {
                    return;
                }

                if (!mUserLearnedDrawer) {
                    // The user manually opened the drawer; store this flag to prevent auto-showing
                    // the navigation drawer automatically in the future.
                    mUserLearnedDrawer = true;
                    SharedPreferences
                            sp =
                            PreferenceManager.getDefaultSharedPreferences(getActivity());
                    sp.edit()
                            .putBoolean(PREF_USER_LEARNED_DRAWER, true)
                            .apply();
                }

                getActivity().invalidateOptionsMenu(); // calls onPrepareOptionsMenu()
            }
        };

        mDrawerLayout.setDrawerListener(mDrawerToggle);
    }




    private void selectItem(int position) {

        if (position != 1) FragmentPosition.INSTANCE.position = position;
        if (mDrawerListView != null) {
            if (position < getActivity().getResources()
                    .getStringArray(
                            R.array.navigation_drawer_items_array
                    ).length) mDrawerListView.setItemChecked(position, true);
        }
        if (mDrawerLayout != null) {
            mDrawerLayout.closeDrawer(mFragmentContainerView);
        }
        if (mCallbacks != null) {
            mCallbacks.onNavigationDrawerItemSelected(position);
        }
    }


    public void closeMenu() {
        if (mDrawerLayout != null) {
            mDrawerLayout.closeDrawer(mFragmentContainerView);
        }
    }


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mCallbacks = (NavigationDrawerCallbacks) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException("Activity must implement NavigationDrawerCallbacks.");
        }
    }


    @Override
    public void onDetach() {
        super.onDetach();
        mCallbacks = null;
    }


    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        // Forward the new configuration the drawer toggle component.
        mDrawerToggle.onConfigurationChanged(newConfig);
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        // If the drawer is open, show the global app actions in the action bar. See also
        // showGlobalContextActionBar, which controls the top-left area of the action bar.
        if (mDrawerLayout != null && isDrawerOpen()) {
            showGlobalContextActionBar();
        }
        super.onCreateOptionsMenu(menu, inflater);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        //
        //        if (item.getItemId() == R.id.action_example) {
        //            Toast.makeText(getActivity(), "Example action.", Toast.LENGTH_SHORT).show();
        //            return true;
        //        }

        return super.onOptionsItemSelected(item);
    }


    /**
     * Per the navigation drawer design guidelines, updates the action bar to show the global app
     * 'context', rather than just what's in the current screen.
     */
    private void showGlobalContextActionBar() {
        ActionBar actionBar = getActionBar();
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
        actionBar.setTitle(R.string.app_name);
    }


    private ActionBar getActionBar() {
        return getActivity().getActionBar();
    }


    /**
     * Callbacks interface that all activities using this fragment must implement.
     */
    public static interface NavigationDrawerCallbacks {

        /**
         * Called when an item in the navigation drawer is selected.
         */
        void onNavigationDrawerItemSelected(int position);

    }

    private final CompoundButton.OnCheckedChangeListener
                                       tbProfileListener  =
            new CompoundButton.OnCheckedChangeListener() {

                @Override
                public void onCheckedChanged(
                        CompoundButton buttonView, boolean isChecked
                ) {
                    if (isChecked) {
                        mDrawerListView.setVisibility(View.GONE);
                        llProfileDetails.setVisibility(View.VISIBLE);
                        llUnLogged.setVisibility(View.VISIBLE);
                        btnBackMenu.setVisibility(View.VISIBLE);

                    } else {
                        llProfileDetails.setVisibility(View.GONE);
                        mDrawerListView.setVisibility(View.VISIBLE);
                        if (LoginState.INSTANCE.login != 1) llUnLogged.setVisibility(View.VISIBLE);
                        else llUnLogged.setVisibility(View.GONE);
                        btnBackMenu.setVisibility(View.GONE);
                    }
                }
            };
    private final View.OnClickListener openAuthentication = new View.OnClickListener() {

        @Override
        public void onClick(View v) {
            if (LoginState.INSTANCE.login == 1) {
                tbProfile.setChecked(false);

                llUnLogged.setVisibility(View.VISIBLE);
                llLogged.setVisibility(View.GONE);
                LoginState.INSTANCE.login = 0;
                LoginState.INSTANCE.profile = new ProfileEntity();
                btnLogin.setText(R.string.log_in);

                CookieManager.getInstance()
                        .removeAllCookie();

            } else {
                ((NavigationDrawerActivity) getActivity()).startAuth();
            }
        }
    };
}
