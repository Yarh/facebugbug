package bigdig.yarh.ellotv.activitytry.ui.fragment;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import bigdig.yarh.ellotv.activitytry.R;
import bigdig.yarh.ellotv.activitytry.ui.activity.NavigationDrawerActivity;

public class AuthenticationFragment
        extends Fragment {

    private EditText etLogin;
    private EditText etPassword;
    private ProgressDialog pd;

    // TODO: Rename and change types of parameters


    public AuthenticationFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState
    ) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.activity_autentication, container, false);

        etLogin = (EditText) view.findViewById(R.id.et_login_name);
        TextView signText = (TextView) view.findViewById(R.id.textView_registrer);
        signText.setText(
                Html.fromHtml(getString(R.string.login_sign_question))
        );
        etPassword = (EditText) view.findViewById(R.id.et_login_password);

        return view;
    }


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        ((NavigationDrawerActivity) activity).onSectionAttached(getArguments().getString("title"));

    }






}
