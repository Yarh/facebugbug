package bigdig.yarh.ellotv.activitytry.ui.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import bigdig.yarh.ellotv.activitytry.R;
import bigdig.yarh.ellotv.data.DeviceType;
import bigdig.yarh.ellotv.data.MenuItemSelected;

public class SplashScreenActivity
        extends Activity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splashscreen);
        DeviceType.INSTANCE.phone = true;
        startActivity(
                new Intent(
                        SplashScreenActivity.this, NavigationDrawerActivity.class
                )
        );
        finish();
    }
}
