package bigdig.yarh.ellotv.activitytry.ui.activity;

import android.app.ActionBar;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import bigdig.yarh.ellotv.activitytry.R;
import bigdig.yarh.ellotv.activitytry.ui.fragment.AuthenticationFragment;
import bigdig.yarh.ellotv.activitytry.ui.fragment.MainFeedFragment;
import bigdig.yarh.ellotv.activitytry.ui.fragment.NavigationDrawerFragment;
import bigdig.yarh.ellotv.data.DeviceType;

import static bigdig.yarh.ellotv.activitytry.R.id.container;

public class NavigationDrawerActivity
        extends FragmentActivity
        implements NavigationDrawerFragment.NavigationDrawerCallbacks {

    /**
     * Fragment managing the behaviors, interactions and presentation of the navigation drawer.
     */
    private NavigationDrawerFragment mNavigationDrawerFragment;

    /**
     * Used to store the last screen title. For use in {@link #restoreActionBar()}.
     */
    private CharSequence mTitle;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (DeviceType.INSTANCE.phone) {
            this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }
        setContentView(R.layout.activity_navigation_drawer);
        getActionBar().setDisplayHomeAsUpEnabled(true);

        //        getActionBar().setIcon(R.drawable.ic_launcher);
        mNavigationDrawerFragment =
                (NavigationDrawerFragment) getSupportFragmentManager().findFragmentById(R.id.navigation_drawer);
        mTitle = getTitle();

        // Set up the drawer.
        mNavigationDrawerFragment.setUp(
                R.id.navigation_drawer, (DrawerLayout) findViewById(R.id.drawer_layout)
        );
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            if (extras.containsKey("auth")) {
                if (extras.getBoolean("auth")) {
                    startAuth();
                }
            }
        }
    }




    public void startAuth() {
        FragmentManager fragmentManager = getSupportFragmentManager();
        Fragment fragment;

        Bundle data = new Bundle();
        fragment = new AuthenticationFragment();
        data.putString("title", "Authentication");
        data.putString("message", "");
        fragment.setArguments(data);
        fragmentManager.beginTransaction()
                .replace(container, fragment)
                .commit();
        mNavigationDrawerFragment.closeMenu();
    }



    //    @Override
    //    public void onActivityResult(int requestCode, int resultCode, Intent data) {
    //        super.onActivityResult(requestCode, resultCode, data);
    //        Log.e("activity result activity", "");
    //
    //    }
    public void favMain(View v) {
        String tag = (String) v.getTag();
        String[] parts = tag.split(":/:");
        Intent sharingIntent = new Intent(Intent.ACTION_SEND);

        sharingIntent.setType("text/plain");
        sharingIntent.putExtra(
                Intent.EXTRA_SUBJECT, parts[0]
        );
        sharingIntent.putExtra(
                Intent.EXTRA_TEXT, "http://ello.tv/video/" + parts[1]
        );
        startActivity(Intent.createChooser(sharingIntent, "Share via"));
    }


    @Override
    public void onNavigationDrawerItemSelected(int position) {
        // update the main content by replacing fragments
        FragmentManager fragmentManager = getSupportFragmentManager();
        String[] title = getResources().getStringArray(R.array.navigation_drawer_items_array);
        String[] urls = getResources().getStringArray(R.array.navigation_drawer_social_link_array);
        Fragment fragment = null;
        Bundle data = new Bundle();
        Uri uri = null;
        Intent intent = null;
        switch (position) {
            case 0:
                fragment = new MainFeedFragment();
                data.putString("title", title[position]);
                fragment.setArguments(data);
                fragmentManager.beginTransaction()
                        .replace(container, fragment)
                        .commit();
                break;


            case 9:

                uri = Uri.parse(urls[0]);
                intent = new Intent(Intent.ACTION_VIEW, uri);
                startActivity(intent);
                break;
            case 10:
                uri = Uri.parse(urls[1]);
                intent = new Intent(Intent.ACTION_VIEW, uri);
                startActivity(intent);
                break;
            case 11:
                uri = Uri.parse(urls[2]);
                intent = new Intent(Intent.ACTION_VIEW, uri);
                startActivity(intent);
                break;
            case 12:
                uri = Uri.parse(urls[3]);
                intent = new Intent(Intent.ACTION_VIEW, uri);
                startActivity(intent);
                break;
            case 13:
                uri = Uri.parse(urls[4]);
                intent = new Intent(Intent.ACTION_VIEW, uri);
                startActivity(intent);
                break;

        }

    }


    public void onSectionAttached(String title) {
        mTitle = getTitle();

        mTitle = title;
    }


    void restoreActionBar() {
        ActionBar actionBar = getActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setTitle(mTitle);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (!mNavigationDrawerFragment.isDrawerOpen()) {
            // Only show items in the action bar relevant to this screen
            // if the drawer is not showing. Otherwise, let the drawer
            // decide what to show in the action bar.
            getMenuInflater().inflate(R.menu.my, menu);
            restoreActionBar();
            return true;
        }
        return super.onCreateOptionsMenu(menu);
    }


}
