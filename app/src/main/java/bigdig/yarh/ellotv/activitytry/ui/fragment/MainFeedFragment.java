package bigdig.yarh.ellotv.activitytry.ui.fragment;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;

import java.util.ArrayList;
import java.util.List;

import bigdig.yarh.ellotv.activitytry.R;
import bigdig.yarh.ellotv.activitytry.ui.activity.NavigationDrawerActivity;
import bigdig.yarh.ellotv.adapter.FragmentMainAdapter;
import bigdig.yarh.ellotv.data.MainEntity;
import bigdig.yarh.ellotv.utils.MaterialRequest;


public class MainFeedFragment
        extends Fragment {
    public MainFeedFragment() {
    }
    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState
    ) {
        View view = inflater.inflate(R.layout.fragment_main_feed, container, false);

        return view;
    }
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        ((NavigationDrawerActivity) activity).onSectionAttached(getArguments().getString("title"));
    }


}
