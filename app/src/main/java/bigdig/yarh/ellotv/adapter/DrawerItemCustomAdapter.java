package bigdig.yarh.ellotv.adapter;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import bigdig.yarh.ellotv.activitytry.R;
import bigdig.yarh.ellotv.data.Local;

public class DrawerItemCustomAdapter extends BaseAdapter {

    private final Context mContext;
    private       String[] data        = null;
    private       String[] social      = null;
    private final int[]
                           sosial_prev =
            {R.drawable.nd_fb, R.drawable.nd_vk, R.drawable.nd_twitter, R.drawable.nd_instagramm, R.drawable.nd_google_pluss};


    @Override
    public int getCount() {

        int count = 0;
        if (!Local.INSTANCE.lang.isEmpty()) {
            count = data.length;
        } else {
            count = data.length + social.length + 1;
        }
        return count;
    }


    @Override
    public String getItem(int position) {
        return data[position];
    }


    @Override
    public long getItemId(int position) {
        return position;
    }


    public DrawerItemCustomAdapter(
            Context mContext, String[] data, String[] social) {

        this.mContext = mContext;
        this.data = data;
        this.social = social;

    }

    @Override
    public int getViewTypeCount() {
        return 3;
    }

    @Override
    public int getItemViewType(int position) {
        if (position < data.length) return 0;
        else if (position == data.length) return 1;
        else return 2;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View rowView = convertView;
        int type = getItemViewType(position);
        if (type == 0) {
            if (rowView == null) {
                LayoutInflater inflater = (LayoutInflater) mContext
                        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

                rowView = inflater.inflate(
                        R.layout
                        .list_item_drawer, parent, false);
                // configure view holder
                ViewHolder viewHolder = new ViewHolder(
                        (TextView) rowView.findViewById(R.id.textViewName), null, (ImageView) rowView.findViewById(R.id.im_divider));
                Typeface fontTitle = Typeface.createFromAsset(inflater.getContext()
                        .getAssets(), "fonts/ubuntu_r.ttf");
                viewHolder.tvTitle.setTypeface(fontTitle);
                viewHolder.tvTitle.setShadowLayer(2f, -1, 1, Color.LTGRAY);
                rowView.setTag(viewHolder);
            }

            // fill data
            ViewHolder holder = (ViewHolder) rowView.getTag();
            if (position < data.length - 1) {
                holder.divider.setVisibility(View.VISIBLE);
            } else holder.divider.setVisibility(View.INVISIBLE);
            holder.tvTitle.setText(Html.fromHtml(data[position]));
        } else if (type == 1) {
            if (rowView == null) {
                LayoutInflater inflater = (LayoutInflater) mContext
                        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                rowView = inflater.inflate(R.layout
                        .list_item_navigation_drawer_follow, parent, false);
                // configure view holder
                ViewHolder viewHolder = new ViewHolder((TextView) rowView.findViewById(R.id.social_text), null,
                        null);
                rowView.setTag(viewHolder);
            }
        } else if (type == 2) {

            LayoutInflater inflater = (LayoutInflater) mContext
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            rowView = inflater.inflate(R.layout
                    .list_item_navigation_drawer_social, parent, false);
            // configure view holder
            ViewHolder viewHolder = new ViewHolder(
                    (TextView) rowView.findViewById(R.id.social_text),
                    (ImageView) rowView.findViewById(R.id.social_preview), (ImageView) rowView.findViewById(R.id.im_divider));
            Typeface fontTitle = Typeface.createFromAsset(inflater.getContext()
                    .getAssets(), "fonts/ubuntu_r.ttf");
            viewHolder.tvTitle.setTypeface(fontTitle);
            viewHolder.tvTitle.setShadowLayer(2f, -1, 1, Color.LTGRAY);
            rowView.setTag(viewHolder);


            // fill data
            ViewHolder holder = (ViewHolder) rowView.getTag();
            if (position < getCount() - 1) {
                holder.divider.setVisibility(View.VISIBLE);
            } else holder.divider.setVisibility(View.INVISIBLE);
            holder.tvTitle.setText(social[position - data.length - 1]);
//            holder.tvTitle.setCompoundDrawablesWithIntrinsicBounds(sosial_prev[position - data.length - 2],0,0,0);
            holder.imPreviewSocial.setImageResource(sosial_prev[position - data.length - 1]);

        }

        return rowView;
    }

    private static class ViewHolder {
        public final TextView tvTitle;
        public final ImageView imPreviewSocial;
        public final ImageView divider;

        public ViewHolder(
                TextView tvTitleSocial, ImageView imPreviewSocial, ImageView divider
        ) {
            this.tvTitle = tvTitleSocial;
            this.imPreviewSocial = imPreviewSocial;
            this.divider = divider;

        }

    }

}