package bigdig.yarh.ellotv.adapter;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.List;

import bigdig.yarh.ellotv.activitytry.R;
import bigdig.yarh.ellotv.data.MainEntity;

public class FragmentMainAdapter extends BaseAdapter {
	private final List<MainEntity> data;
	private final Context context;
	private final ImageLoader imageLoader;

	public FragmentMainAdapter(Context activity, List<MainEntity> data) {
		this.data = data;
		this.context = activity;
		imageLoader = ImageLoader.getInstance();

	}


	public int getCount() {
		return data.size();
	}

	public Object getItem(int position) {
		return position;
	}

	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View rowView = convertView;
		// reuse views
		if (rowView == null) {
			LayoutInflater inflater = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

			rowView = inflater.inflate(R.layout.list_item_main_feed, parent,false);
			// configure view holder
			ViewHolder viewHolder = new ViewHolder(
					(TextView) rowView.findViewById(R.id.tv_title),
					(TextView) rowView.findViewById(R.id.tv_artist),
					(ImageView) rowView.findViewById(R.id.im_back),
                    (TextView) rowView.findViewById(R.id.tv_view_count),
                    (TextView) rowView.findViewById(R.id.tv_like_count),
                    (ImageButton) rowView.findViewById(R.id.ibtn_share));
			Typeface fontTitle = Typeface.createFromAsset(inflater.getContext().getAssets(), "fonts/ubuntu_r.ttf");
			viewHolder.title.setTypeface(fontTitle);
			Typeface fontArtist = Typeface.createFromAsset(inflater.getContext().getAssets(), "fonts/ubuntu_l.ttf");
			viewHolder.artist.setTypeface(fontArtist);
			viewHolder.title.setShadowLayer(2f, -1, 1, Color.LTGRAY);
			viewHolder.artist.setShadowLayer(2f, -1, 1, Color.LTGRAY);
            viewHolder.viewCount.setTypeface(fontArtist);
            viewHolder.likeCount.setTypeface(fontArtist);
            viewHolder.likeCount.setShadowLayer(2f, -1, 1, Color.LTGRAY);
            viewHolder.viewCount.setShadowLayer(2f, -1, 1, Color.LTGRAY);
            viewHolder.ibtnShare.setFocusable(false);
			rowView.setTag(viewHolder);
		}

		// fill data
		ViewHolder holder = (ViewHolder) rowView.getTag();
		holder.title.setText(data.get(position).getTitle());
        holder.viewCount.setText("" + data.get(position).getViewCount());
        holder.likeCount.setText("" + data.get(position).getFavoriteCount());
		holder.artist.setText(data.get(position).getArtist());
		String value = data.get(position).getImageUrl();
        String tag = data.get(position).getTitle()+ ":/:"+ data.get(position).getSlug();
		imageLoader.displayImage(value, holder.imBack);
		holder.ibtnShare.setTag(tag);
		return rowView;
	}


	private static class ViewHolder {
		final TextView title;
		final TextView artist;
		final ImageView imBack;
        final TextView viewCount;
        final TextView likeCount;
        final ImageButton ibtnShare;


        private ViewHolder(TextView title, TextView artist, ImageView imBack,TextView viewCount,TextView likeCount,ImageButton ibtnShare) {
			this.title = title;
			this.artist = artist;
			this.imBack = imBack;
            this.likeCount = likeCount;
            this.viewCount = viewCount;
            this.ibtnShare=ibtnShare;

		}
	}
}
