package bigdig.yarh.ellotv.data;

import java.util.Map;

public class ProfileEntity {
	private int id = 0;
	private String nickName = "";
	private String firstNam = "";
	private String lastName = "";
	private String email = "";
	private int gender = 0;
	private int country = 0;
	private String city = "";
	private int year = 0;
    private int month = 0;
	private int day = 0;
	private String avatar = "";
	private Map<String, Integer> social = null;
	private boolean subscribe = false;
	private int followers_count = 0;
	private int following_count = 0;


    public void setId(int id) {
		this.id = id;
	}


    public void setNickName(String nickName) {
		this.nickName = nickName;
	}

	public String getFirstNam() {
		return firstNam;
	}

	public void setFirstNam(String firstNam) {
		this.firstNam = firstNam;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}


    public void setEmail(String email) {
		this.email = email;
	}


    public void setGender(int gender) {
		this.gender = gender;
	}


    public void setCountry(int country) {
		this.country = country;
	}


    public void setCity(String city) {
		this.city = city;
	}


    public String getAvatar() {
		return avatar;
	}

	public void setAvatar(String avatar) {
		this.avatar = avatar;
	}


    public void setSocial(Map<String, Integer> social) {
		this.social = social;
	}


    public void setSubscribe(boolean subscribe) {
		this.subscribe = subscribe;
	}

	public int getFollowers_count() {
		return followers_count;
	}

	public void setFollowers_count(int followers_count) {
		this.followers_count = followers_count;
	}

	public int getFollowing_count() {
		return following_count;
	}

	public void setFollowing_count(int following_count) {
		this.following_count = following_count;
	}

}
